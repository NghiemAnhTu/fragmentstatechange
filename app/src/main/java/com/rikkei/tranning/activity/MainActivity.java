package com.rikkei.tranning.activity;

import android.media.MediaPlayer;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    Fragment fragment;
    int time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragment = new Fragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.layout, fragment);
        fragmentTransaction.commit();

        fragment.mp3 = MediaPlayer.create(MainActivity.this, R.raw.test);
        fragment.mp3.start();


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fragment.mp3.pause();
        time = fragment.mp3.getCurrentPosition();
        outState.putInt("timeline", time);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        time = savedInstanceState.getInt("timeline");
        fragment.mp3.seekTo(time);
        fragment.mp3.start();
        super.onRestoreInstanceState(savedInstanceState);
    }
}
